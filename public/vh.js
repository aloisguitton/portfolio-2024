const setVH = () => {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
};

setVH();

if (typeof window !== undefined) {
  window.addEventListener("resize", () => {
    setVH();
  });
}

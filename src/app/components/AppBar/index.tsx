import React from "react";
import { Typography } from "@mui/material";
import styles from "./style.module.scss";

const AppBar = () => (
  <div className={styles.appbar}>
    <div className={styles.title}>
      <div>
        <Typography variant="h1" className={styles.name}>
          Aloïs Guitton
        </Typography>
        <Typography variant="h2" className={styles.title}>
          Développeur FullStack JS | Freelance
        </Typography>
      </div>
    </div>
  </div>
);

export default AppBar;

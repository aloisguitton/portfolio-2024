"use client";

import { Avatar, Chip, IconButton, Typography } from "@mui/material";
import React, { useState } from "react";

import Head from "next/head";
import Image from "next/legacy/image";
import { KeyboardArrowDown } from "@mui/icons-material";
import clsx from "clsx";
import styles from "./style.module.scss";

const technos = {
  reactJS: {
    icon: "/illustrations/technos/reactjs.png",
    text: "ReactJS",
  },
  reactNative: {
    icon: "/illustrations/technos/reactjs.png",
    text: "React Native",
  },
  graphql: {
    icon: "/illustrations/technos/graphql.png",
    text: "GraphQL",
  },
  serverless: {
    icon: "/illustrations/technos/lambda.png",
    text: "Serverless",
  },
  knex: {
    icon: "/illustrations/technos/knex.png",
    text: "Knex",
  },
  typescript: {
    icon: "/illustrations/technos/ts.png",
    text: "Typescript",
  },
  vitest: {
    icon: "/illustrations/technos/vitest.png",
    text: "Vitest",
  },
  slack: {
    icon: "/illustrations/technos/slack.png",
    text: "Slack",
  },
  git: {
    icon: "/illustrations/technos/git.png",
    text: "Git",
  },
  notion: {
    icon: "/illustrations/technos/notion.png",
    text: "Notion",
  },
  express: {
    icon: "/illustrations/technos/expressjs.png",
    text: "ExpressJS",
  },
  nextJS: {
    icon: "/illustrations/technos/nextjs.svg",
    text: "NextJS",
  },
  prisma: {
    icon: "/illustrations/technos/prisma.png",
    text: "Prisma",
  },
  expo: {
    icon: "/illustrations/technos/expo.png",
    text: "Expo",
  },
  angular: {
    icon: "/illustrations/technos/angular.png",
    text: "Angular",
  },
  elastic: {
    icon: "/illustrations/technos/elastic.png",
    text: "Elasticsearch",
  },
  kibana: {
    icon: "/illustrations/technos/kibana.svg",
    text: "Kibana",
  },
  jira: {
    icon: "/illustrations/technos/jira.svg",
    text: "Jira",
  },
  teams: {
    icon: "/illustrations/technos/teams.png",
    text: "Teams",
  },
  uipath: {
    icon: "/illustrations/technos/uipath.png",
    text: "UiPath",
  },
};

const experiences = [
  {
    title: "Tech-Wise | Fulll - Ingénieur Fullstack JS",
    image: (
      <Image
        src="/illustrations/techwise_sas_logo.jpeg"
        alt="Aloïs Guitton | Tech-wise - SAS | Fulll"
        layout="fill"
        objectFit="contain"
      />
    ),
    subtitle:
      "2023 - Aujourd'hui : Ingénieur Fullstack JS & Architecte Logiciel AWS",
    content: (
      <>
        <div className={styles.section}>
          <Typography variant="h4">Développeur Fullstack JavaScript</Typography>
          <Typography>
            Utilisation de NodeJS et de serverless pour créer des fonctions
            lambda permettant d&apos;interroger nos données et d&apos;effectuer
            des calcules complexes.
          </Typography>
          <Typography>
            Utilisation de ReactJS et GraphQL pour présenter la donnée.
          </Typography>
        </div>

        <div className={styles.section}>
          <Typography variant="h4">Architecte AWS</Typography>
          <Typography>
            Utilisation avancée des services AWS pour le déploiement et la
            gestion d&apos;applications cloud-native.
          </Typography>
          <Typography>
            Utilisation de Terraform pour la gestion d&apos;infrastructure,
            permettant une approche déclarative et reproductible du déploiement
            AWS.
          </Typography>
        </div>

        <div className={styles.section}>
          <Typography variant="h4">Outils et Collaboration</Typography>
          <Typography>
            Utilisation des outils de gestion de version Git pour le contrôle de
            code source et la collaboration.
          </Typography>
          <Typography>
            Utilisation efficace d&apos;outils de communication tels que Slack
            et Notion pour la collaboration d&apos;équipe.
          </Typography>
        </div>
      </>
    ),
    technos: [
      {
        title: "Front-end",
        items: [technos.reactJS, technos.graphql],
      },
      {
        title: "Back-end",
        items: [
          technos.typescript,
          technos.serverless,
          technos.vitest,
          technos.knex,
          technos.graphql,
        ],
      },
      {
        title: "Outils collaboratifs",
        items: [technos.slack, technos.notion, technos.git],
      },
    ],
  },
  {
    title: "Développeur Notion",
    image: (
      <Image
        src="/illustrations/notion-logo.png"
        alt="Aloïs Guitton | Notion"
        layout="fill"
        objectFit="contain"
      />
    ),
    subtitle:
      "Fin 2023 - Aujourd'hui : Freelance développement de template Notion",
    content: (
      <div>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
      </div>
    ),
    technos: [
      {
        title: "Notion",
        items: [technos.notion],
      },
    ],
  },
  {
    title: "Partielo",
    image: (
      <Image
        src="/illustrations/partielo-logo.png"
        alt="Aloïs Guitton | Partielo "
        layout="fill"
        objectFit="contain"
      />
    ),
    subtitle: "Février 2021 - Aujourd'hui : CTO & associé",
    content: (
      <div>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
      </div>
    ),
    technos: [
      {
        title: "Front-end",
        items: [technos.nextJS, technos.reactJS, technos.graphql],
      },
      {
        title: "Back-end",
        items: [
          technos.typescript,
          technos.serverless,
          technos.prisma,
          technos.express,
          technos.graphql,
        ],
      },
      {
        title: "Application mobile",
        items: [technos.reactNative, technos.expo, technos.graphql],
      },
      {
        title: "Outils collaboratifs",
        items: [technos.git],
      },
    ],
  },
  {
    title: "Wedrivit",
    image: (
      <Image
        src="/illustrations/wedrivit-logo.png"
        alt="Aloïs Guitton | Wedrivit"
        layout="fill"
        objectFit="contain"
      />
    ),
    subtitle: "Janvier 2020 - Aujourd'hui : CTO & associé",
    technos: [
      {
        title: "Front-end",
        items: [technos.nextJS, technos.reactJS, technos.graphql],
      },
      {
        title: "Back-end",
        items: [
          technos.typescript,
          technos.prisma,
          technos.express,
          technos.graphql,
        ],
      },
      {
        title: "Outils collaboratifs",
        items: [technos.notion, technos.slack, technos.git],
      },
    ],
    content: (
      <div>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
      </div>
    ),
  },
  {
    title: "La Banque Postale",
    image: (
      <Image
        src="/illustrations/lbp-logo.png"
        alt="Aloïs Guitton | Wedrivit"
        layout="fill"
        objectFit="contain"
      />
    ),
    subtitle:
      "Septembre 2019 - Août 2022 : Alternant ingénieur intégrateur d'application",
    technos: [
      {
        title: "Robotic Process Automation (RPA)",
        items: [technos.uipath],
      },
      {
        title: "Outils collaboratifs",
        items: [technos.teams, technos.jira, technos.git],
      },
    ],
    content: (
      <div>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
      </div>
    ),
  },
  {
    title: "Orange",
    image: (
      <Image
        src="/illustrations/orange-logo.png"
        alt="Aloïs Guitton | Wedrivit"
        layout="fill"
        objectFit="contain"
      />
    ),
    subtitle:
      "Août 2017 - Juin 2019 : Alternant administrateur système et réseau",
    content: (
      <div>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
        <Typography color="white">aeazeaz</Typography>
      </div>
    ),
    technos: [
      {
        title: "Monitoring & Big Data",
        items: [technos.elastic, technos.kibana],
      },
      {
        title: "Front-end",
        items: [technos.angular],
      },
      {
        title: "Outils collaboratifs",
        items: [technos.teams, technos.git],
      },
    ],
  },
];

export default function Home() {
  const [open, setOpen] = useState<number[]>(experiences.map((_, i) => i));

  return (
    <>
      <Head>
        <title>Portfolio | Aloïs Guitton</title>
        <meta
          name="description"
          content="Aloïs Guitton, je suis développeur FullStack JavaScript(JS) et Freelance"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.container}>
        <div className={styles.exp}>
          {experiences.map((content, index) => (
            <div
              key={content.title}
              className={clsx(
                styles.item,
                open.includes(index) ? styles.open : undefined
              )}
            >
              <div
                className={styles.title}
                onClick={() =>
                  setOpen((o) =>
                    o.includes(index)
                      ? o.filter((i) => i !== index)
                      : [...o, index]
                  )
                }
                role="button"
              >
                <IconButton>
                  <KeyboardArrowDown />
                </IconButton>
                <Typography color={"white"} variant="h2">
                  {content.title}
                </Typography>
                {content.image ? (
                  <div className={styles.img}>
                    <div>{content.image}</div>
                  </div>
                ) : null}
              </div>
              <div className={styles.details}>
                <Typography
                  variant="h3"
                  color={"white"}
                  className={styles.subtitle}
                >
                  {content.subtitle}
                </Typography>

                <div className={styles.content}>
                  <div className={styles.technosList}>
                    {content.technos.map((t) => (
                      <div
                        key={`${index}-${t.items.map((i) => i.text).join("")}`}
                        className={styles.technos}
                      >
                        <Typography>{t.title} :</Typography>
                        <div className={styles.chips}>
                          {t.items.map((i) => (
                            <Chip
                              className={styles.chip}
                              size="small"
                              variant="outlined"
                              key={i.text}
                              avatar={<Avatar src={i.icon} />}
                              label={i.text}
                            />
                          ))}
                        </div>
                      </div>
                    ))}
                  </div>

                  {content.content}
                </div>
              </div>
            </div>
          ))}
          <div className={clsx(styles.contact, styles.item)}>
            <Typography color={"white"} variant="h2">
              Aloïs Guitton
            </Typography>

            <a href="mailto:aloisguitton@orange.fr">
              <Typography>aloisguitton@orange.fr </Typography>
            </a>

            <a href="tel:(+33)672551697">
              <Typography>(+33) 6 72 55 16 97 </Typography>
            </a>

            <a href="https://www.linkedin.com/in/aloisguitton/" target="_blank">
              <div className={styles.img}>
                <Image
                  src="/illustrations/linkedin.png"
                  alt="Aloïs Guitton | Linkedin"
                  layout="fill"
                  objectFit="contain"
                />
              </div>
            </a>
          </div>
        </div>
      </div>
    </>
  );
}
